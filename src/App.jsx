import React from "react";
import { Navbar } from "./components/Navbar";
import FirstSreen from "./components/FirstSreen";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import { Connexion } from "./pages/auth/Connexion";
import { About } from "./pages/auth/About";
import { Contact } from "./pages/auth/Contact";
import { NoPage } from "./pages/auth/NoPage";
import Dashboard from "./components/Dashboard";
// import Accueil from "./pages/Accueil";
// import { Mesrv } from "./pages/Mesrv";
// import { Patient } from "./pages/Patient";
// import { Compte } from "./pages/Compte";
import { SecondDashboard } from "./components/SecondDashboard";
import { Accueils } from "./pages/pagesDashboard/Accueils";
import { Mesrv } from "./pages/pagesDashboard/Mesrv";
import { Mespatients } from "./pages/pagesDashboard/Mespatients";
import { Moncompte } from "./pages/pagesDashboard/Moncompte";

function App() {
  return (
    <>
      <BrowserRouter>
        <Routes>
          {/* home */}
          <Route path="/" element={<Navbar />}>
            <Route index element={<FirstSreen />} />
            <Route path="/about" element={<About />} />
            <Route path="/contact" element={<Contact />} />
            <Route path="/connexion" element={<Connexion />} />
            <Route path="*" element={<NoPage />} />
          </Route>

          {/* dashboard */}

          <Route path="/dashbord" element={<SecondDashboard />}>
            <Route index element={<Accueils />} />
            <Route path="/dashbord/rv" element={<Mesrv />} />
            <Route path="/dashbord/patients" element={<Mespatients />} />
            <Route path="/dashbord/compte" element={<Moncompte />} />
          </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;

{
  /* <Route path="/dashbord/accueil" element={<Accueil />} />
<Route path="/dashbord/rv" element={<Mesrv />} />
<Route path="/dashbord/patient" element={<Patient />} />
<Route path="/dashbord/compte" element={<Compte />} /> */
}
